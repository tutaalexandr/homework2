  import Vue from "vue";
  import axios from "axios";
  import toasted from 'vue-toasted';
  export default {
    state:{
      items: [],
    },
    mutations:{
      addItem(store, item) {
        store.items.push(item);
      },
      cleanItem(store) {
        store.items = [];
      },
    },
    getters:{
      getItems(state) {
        return state.items;
      }
    },
    actions:{
      fetchItems: (store) => {
        axios
            .get('http://kinoha.itis.today/api/history')
            .then(response => {
              response.data.forEach(item => store.commit('addItem', item));
            });
      },
      pushItem: (store, item) => {
        axios.post('http://kinoha.itis.today/api/history', item)
            .then(response => {
                if (response.data.success === true) {
                    Vue.toasted.success('Data is save');
                    store.commit('cleanItem');
                    store.dispatch('fetchItems');
                } else {
                    Vue.toasted.error('Api server retutn' + response.data);
                }
            });
      },
      deleteItem: (store, item) => {
        axios.delete('http://kinoha.itis.today/api/history/' + item.id)
            .then(response => {
                if (response.data.success === true) {
                    Vue.toasted.success('Data is change');
                    store.commit('cleanItem');
                    store.dispatch('fetchItems');
                } else {
                    Vue.toasted.error('Api server retutn' + response.data);
                }

            });
      }
    }
  }


