import Vue from 'vue';
import Vuex from 'vuex';
import App from './App';
import store from './Store';
import Toasted from 'vue-toasted';

Vue.use(Vuex);
Vue.use(Toasted);

window.app = new Vue({
    store: new Vuex.Store(store),
    mounted() {
        this.$store.dispatch('fetchItems');
    },
    render: h => h(App),
}).$mount('#app');
